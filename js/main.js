function validate_number(evt)
{
	var theEvent = evt || window.event; 
	var key = theEvent.keyCode || theEvent.which; 
	key = String.fromCharCode(key); 
	// Alleen getalen tab en backspace
	var regex = /[0-9\t\b]/; 
	if (!regex.test(key))
	{ 
		theEvent.returnValue = false; 
		if(theEvent.preventDefault) theEvent.preventDefault(); 
	} 
}
  	
function add_to_cart(sender)
{
	var value = sender.value;
	if (value != '' && value > 0)
	{
		var rowIndex = sender.parentNode.parentNode.rowIndex - 1;
		
		var table = sender.parentNode.parentNode.parentNode;
		var selectedRow = table.getElementsByTagName('tr')[rowIndex];
		
		var articleName = selectedRow.getElementsByTagName('td')[0].innerHTML;
		var selectedAmount = selectedRow.getElementsByTagName('td')[3].getElementsByTagName('input')[0].value;
		var pricePerUnit = parseFloat(selectedRow.getElementsByTagName('td')[1].innerHTML.replace(',', '.'));
		
		//-------- CHECK IF EXISTS & DELETE
		var tableCart = document.getElementById('shoppingCart');	
		for (var i = 0, row; row = tableCart.rows[i]; i++)
		{
			var articleID = row.cells[0].getAttribute('data-id');
			if (articleID == rowIndex)
			{
				tableCart.deleteRow(row.rowIndex);
			}	
		}
				
		//-------- ADD ROW
		var rowCart = tableCart.insertRow(1);
		
		var articleCartCell = rowCart.insertCell(0);
		var amountCartCell = rowCart.insertCell(1);
		var pricePerUnitCartCell = rowCart.insertCell(2);
		var totalPriceCartCell = rowCart.insertCell(3);
		
		var deleteCell = rowCart.insertCell(4);
		deleteCell.outerHTML = '<td><a href="#" onclick="remove_from_cart(this)"><img src="images/delete.png" border="0" alt="Delete" class="delete_image" /></a></td>';
		
		articleCartCell.innerHTML = articleName;
		articleCartCell.setAttribute('data-id', rowIndex);
		
		amountCartCell.innerHTML = selectedAmount;
		pricePerUnitCartCell.innerHTML = pricePerUnit.toFixed(2);
		var totalPrice = parseFloat(pricePerUnit * selectedAmount).toFixed(2);
		totalPriceCartCell.innerHTML = totalPrice;
		
		recalc_totalShoppingCarPrice();
	}
}

function remove_from_cart(row)
{
	var rowIndex = row.parentNode.parentNode.rowIndex;
	
	var table = document.getElementById('shoppingCart');
	table.deleteRow(rowIndex);
	
	recalc_totalShoppingCarPrice();
}

function recalc_totalShoppingCarPrice()
{
	var table = document.getElementById('shoppingCart');
	var total = 0;
	
	for (var i = 0, row; row = table.rows[i]; i++)
	{
		if (row.rowIndex != 0 && row.rowIndex != (table.rows.length-1))
		{
			var totalRowPrice = parseFloat(row.cells[3].innerHTML).toFixed(2);
			total = parseFloat(total) + parseFloat(totalRowPrice);
		}
	}
	
	var totalCell = document.getElementById('totalCartPrice').innerHTML = parseFloat(total).toFixed(2);
	
	shoppingCart_to_Cookie();
}

function shoppingCart_to_Cookie()
{
	var obj = {}
	obj.data = document.getElementById('shoppingCart').outerHTML;
	
	var JSONStringData = JSON.stringify(obj);
	setCookie('shoppingData', JSONStringData, 15);
}

function deleteCookie(c_name)
{
	setCookie(c_name, '', -1);
}

function getCookie(c_name)
{
	var c_value = document.cookie;
	var c_start = c_value.indexOf(" " + c_name + "=");
	
	if (c_start == -1)
	{
		c_start = c_value.indexOf(c_name + "=");
	}
	
	if (c_start == -1)
	{
		c_value = null;
	}
	else
	{
		c_start = c_value.indexOf("=", c_start) + 1;
		var c_end = c_value.indexOf(";", c_start);
		
		if (c_end == -1)
		{
			c_end = c_value.length;
		}
		
		c_value = unescape(c_value.substring(c_start,c_end));
	}
	return c_value;
}

function load_shopping_data()
{
	var cookie = getCookie('shoppingData');
	
	if (cookie == null)
	{
		setCookie('shoppingData', '', 15);
		return;
	}
	
	var JSONObj = JSON.parse(cookie);
	
	var table = document.getElementById('shoppingCart');
	table.outerHTML = JSONObj.data;
}

function check_valid_input(input)
{
	var value = input.value;
	
	if (value == '')
	{
		input.className = 'error';
		return false;
	}
	
	input.className = '';
	return true;
}

function place_order()
{
	var name_valid = check_valid_input(document.getElementById('inputName'));
	var street_valid = check_valid_input(document.getElementById('inputStreet'));
	var city_valid = check_valid_input(document.getElementById('inputCity'));
	var phone_valid = check_valid_input(document.getElementById('inputPhone'));
	
	if (name_valid && city_valid && street_valid && phone_valid)
	{
		document.getElementById('errorMsg').style.display = 'none';
		
		deleteCookie('shoppingData');
		alert('Bestelling is met success afgerond.');
		window.location.reload(); 
		
		return true;
	}
	else
	{
		document.getElementById('errorMsg').style.display = 'block';
		return false;
	}
}

function setCookie(c_name, value, exdays)
{
	var exdate = new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var c_value = value + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
	document.cookie = c_name + "=" + c_value;
}
